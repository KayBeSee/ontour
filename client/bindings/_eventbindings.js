  module.exports = {
    'model._id': {
      hook: '_id'
    },
    'model.bitId': {
      hook: 'bitId'
    },
    'model.url': {
      hook: 'url'
    },
    'model.datetime': {
      hook: 'datetime'
    },
    'model.ticket_url': {
      hook: 'ticket_url'
    },
    'model.artists.name': {
      hook: 'artists_name'
    },
    'model.artists.url': {
      hook: 'artists_url'
    },
    'model.artists.mbid': {
      hook: 'artists_mbid'
    },
    'model.venue.id': {
      hook: 'venue_id'
    },
    'model.venue.url': {
      hook: 'venue_url'
    },
    'model.venue.name': {
      hook: 'venue_name'
    },
    'model.venue.city': {
      hook: 'venue_city'
    },
    'model.venue.region': {
      hook: 'venue_region'
    },
    'model.venue.country': {
      hook: 'venue_country'
    },
    'model.venue.latitude': {
      hook: 'venue_latitude'
    },
    'model.venue.longitude': {
      hook: 'venue_longitude'
    },
    'model.ticket_status': {
      hook: 'ticket_status'
    },
    'model.on_sale_datetime': {
      hook: 'on_sale_datetime'
    }
  };